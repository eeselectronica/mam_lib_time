/*
* mam_time.h
*
* created: 16/02/2022 02:51:48 p. m.
*  author: labees4
*/


#ifndef mam_time_h_
#define mam_time_h_
 #ifdef __cplusplus
 extern "C"
 {
	 #endif /* __cplusplus */


#include <stdio.h>
#include <stdint.h>
#define SECONDS_PER_DAY 86400L				///< 60 * 60 * 24
#define SECONDS_FROM_1970_TO_2000 946684800 ///< Unixtime for 2000-01-01 00:00:00, useful for initialization
#define pgm_read_byte(addr) (*(const unsigned char *)(addr))

typedef struct
{
	uint32_t unixtime;
	uint16_t yOff;			///< Year offset from 2000
	uint8_t m;				///< Month 1-12
	uint8_t d;				///< Day 1-31
	uint8_t hh;				///< Hours 0-23
	uint8_t mm;				///< Minutes 0-59
	uint8_t ss;				///< Seconds 0-59
	
}mam_date_time_t;

 void mam_make_unixtime(mam_date_time_t * const me);
 void mam_fill_date_time(mam_date_time_t * const me, uint32_t t);


	#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* MAM_TIME_H_ */



