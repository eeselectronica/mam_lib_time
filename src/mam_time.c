#include "mam_time.h"


const uint8_t daysInMonth[12]= {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30};

/*!
@brief  Given a date, return number of days since 2000/01/01,
valid for 2000--2099
@param y Year
@param m Month
@param d Day
@return Number of days
*/
/**************************************************************************/
static uint16_t date2days(mam_date_time_t * const me) {
	

	if (me->yOff >= 2000U)
	{
		
		me->yOff -= 2000U;
	}
	uint16_t days = me->d;
	for (uint8_t i = 1; i < me->m; ++i)
	{
		days += pgm_read_byte(daysInMonth + i - 1);
	}
	if (me->m > 2 && me->yOff % 4 == 0)
	{
		++days;
	}
	
	return days + 365 * me->yOff + (me->yOff + 3) / 4 - 1;
	
}

/**************************************************************************/
/*!
@brief  Given a number of days, hours, minutes, and seconds, return the
total seconds
@param days Days
@param h Hours
@param m Minutes
@param s Seconds
@return Number of seconds total
*/
/**************************************************************************/
static uint32_t time2ulong(mam_date_time_t * const me,uint16_t days )
{
	return ((days * 24UL + me->hh) * 60 + me->mm) * 60 + me->ss;

}


/**************************************************************************/
/*!
@brief  Return Unix time: seconds since 1 Jan 1970.
@see The `DateTime::DateTime(uint32_t)` constructor is the converse of
this method.
@return Number of seconds since 1970-01-01 00:00:00.
*/
/**************************************************************************/
void mam_make_unixtime(mam_date_time_t * const me){
	if(me->m == 0 && me->d == 0 )
	{
		me->unixtime = 0;
		return  me->unixtime;
	}
	volatile uint16_t days = date2days(me);
	me->unixtime = time2ulong(me,days);
	me->unixtime += SECONDS_FROM_1970_TO_2000;
}


void mam_fill_date_time(mam_date_time_t * const me, uint32_t t) {
	me->unixtime = t;
	t -= SECONDS_FROM_1970_TO_2000;
	me->ss = t % 60;
	t /= 60;
	me->mm = t % 60;
	t /= 60;
	me->hh = t % 24;
	uint16_t days = t / 24;
	uint8_t leap;
	for (me->yOff = 0;; ++me->yOff) {
		leap = me->yOff % 4 == 0;
		if (days < 365U + leap)
		break;
		days -= 365 + leap;
	}
	for (me->m = 1; me->m < 12; ++me->m) {
		uint8_t daysPerMonth = pgm_read_byte(daysInMonth + me->m - 1);
		if (leap && me->m == 2)
		++daysPerMonth;
		if (days < daysPerMonth)
		break;
		days -= daysPerMonth;
	}
	me->d = days + 1;
}